# Создайте функцию-генератор чисел Фибоначчи. Примените к ней декоратор, который будет оставлять в
# последовательности только чётные числа.
def decorator(fn_):

    def dec(fn):

        def decor(*args, **kwargs):
            # sp = []
            # for arg in fn(*args):
            #     if arg % 2 == 0:
            #         sp.append(arg)
            # return sp
            return [arg for arg in fn(*args) if arg % 2 == 0]

        return decor

    return dec

"""Ужас сколько сил потратил на этот декоратор.
   Спасибо что ответили на прошлый вопрос :)"""


@decorator(list)
def fib(n):
    a, b = 0, 1
    for _ in range(n+1):
        yield a
        a, b = b, b + a

print(fib(22))

#iter_fib = iter(fib(100))
#print([i for i in iter_fib if i % 2 == 0])


