# Сделать таблицу для подсчёта личных расходов со следующими полями: id, назначение, сумма,
# время.
import sqlite3
import datetime

t = datetime.datetime.now()
with sqlite3.connect('db_for4.1.db') as db:
    cursor = db.cursor()
    #query = '''CREATE TABLE IF NOT EXISTS expenses (id INTEGER, purpose TEXT, sum INTEGER, time TEXT)'''
    query = '''INSERT INTO expenses (id, purpose, sum, time) VALUES (1, 'театр', 600, "12:30")'''
    query1 = '''INSERT INTO expenses (id, purpose, sum, time) VALUES (2, 'бензин', 3000, "15:40")'''
    query2 = '''INSERT INTO expenses VALUES (3, 'кино', 200, "20:00")'''

    cursor.execute(query)
    cursor.execute(query1)
    cursor.execute(query2)
    db.commit()