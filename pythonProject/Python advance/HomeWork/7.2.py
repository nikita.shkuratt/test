# Создайте два класса Directory (папка) и File (файл) с типами (аннотацией).
# Класс Directory должен иметь следующие поля:
# • Название (name типа str);
# • Родительская папка (root типа Directory);
# • Список файлов (files типа список, состоящий из экземпляров File)
# • Список подпапок (sub_directories типа список, состоящий из экземпляров Directory).
# Класс Directory должен иметь следующие поля:

# • Добавление папки в список подпапок (add_sub_directory принимающий экземпляр
# Directory и присваивающий поле root для принимаемого экземпляра);

# • Удаление папки из списка подпапок (remove_sub_directory, принимающий экземпляр
# Directory и обнуляющий у него поле root. Метод также удаляет папку из списка
# sub_directories).

# • Добавление файла в папку (add_file, принимающий экземпляр File и присваивающий ему
# поле directory - см. класс File ниже);

# • Удаление файла из папки (remove_file, принимающий экземпляр File и обнуляющий у него
# поле directory. Метод удаляет файл из списка files).


class Directory:
    def __init__(self, name: str):
        self.name = name

    def remove_file(self, instance: 'File'):
        pass

    def add_file(self, instance: 'File') -> 'Directory':
        pass

    def add_sub_directory(self, instance: 'Directory') -> 'root('')':
        pass

    def remove_sub_directory(self, instance: 'Directory'):
        pass

    def sub_directories(self, instance: 'Directory'):
        pass

    def files(self, instance: 'File'):
        pass

    def root(self, instance: 'Directory'):
        pass

# Класс File должен иметь следующие поля:
# • Название (name типа str);
# • Папка (directory типа Directory)

class File:
    def __init__(self, name: str, directory: 'Directory'):
        self.name_file = name
        self.directory = directory


'''Не понял как сделать правильно. Сбросьте пример если вас есть'''