# Создайте список целых чисел. Получите список квадратов нечётных чисел из этого списка.
import functools
arr = [1, 3, 5, 7, 10, 20, 34, 48, 60]
print([i*i for i in arr])
print([i*i for i in range(100) if i % 2 != 0])
