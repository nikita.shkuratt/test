# Создайте XML файл с вложенными элементами и воспользуйтесь языком поиска XPATH.
# Попробуйте осуществить поиск содержимого по созданному документу XML,
# усложняя свои запросы и добавляя новые элементы, если потребуется.
from xml.etree import ElementTree as ET

"""test = [{'first': 1,
         'second': 2,
         'third': 3,
         'fourth': 4,
         'fifth': 5}]

root = ET.Element('digits')
root.append(ET.Comment('check comment'))

for item in test:
    record = ET.SubElement(root, 'dig1')
    for key, value in item.items():
        e = ET.SubElement(record, key)
        e.text = str(value)

tree = ET.ElementTree(root)
tree.write('for_3.2.xml', encoding='utf8')
ET.dump(root)"""

tree = ET.parse('for_3.2.xml')
root = tree.getroot()
ET.dump(root)
for data in root:
    print(data.find('first').text)
    print(data.find('second').text)
    print(data.find('third').text)
    print(data.find('fourth').text)
    print(data.find('fifth').text)

for_find_all = root.findall('./digits')
for i in root:
    for child in i:
        print('{}: {}'.format(child.tag, child.text))




