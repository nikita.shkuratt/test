# Создать функцию, которая принимает список из элементов типа int, а возвращает новый список
# из строковых значений исходного массива. Добавить аннотацию типов для входных и
# результирующих значений функции.
from typing import TypeVar

T = TypeVar('T')
test_lsy = [1, 2, 4, 5, 7, 10]


def val_int_to_str(int_list: list) -> T:
    return [str(i) for i in int_list]

print(val_int_to_str(test_lsy))
