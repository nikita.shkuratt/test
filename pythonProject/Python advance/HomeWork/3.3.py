# Поработайте с созданием собственных диалектов, произвольно выбирая правила для CSV файлов.
# Зарегистрируйте созданные диалекты и поработайте, используя их, с созданием/чтением файлом.
import csv
class Dialect(csv.Dialect):
    skipinitialspace = 'a'
    delimiter = ','
    quoting = csv.QUOTE_NONNUMERIC
    quotechar = '"'
    lineterminator = '\n'


csv.register_dialect('test_dial', Dialect)
with open('for_3.3.csv', 'w') as f:
    writer = csv.DictWriter(f, ['phones', 'cars'], dialect="test_dial")
    writer.writeheader()
    writer.writerow({
        'phones': 'Iphone',
        'cars': 'bmw'
    })
    writer.writerow({
        'phones': 'samsung',
        'cars': 'mercedes'
    })
    writer.writerow({
        'phones': 'xiaomi',
        'cars': 'vaz'
    })
with open('for_3.3.csv', 'r') as f:
    dict_reader = csv.DictReader(f)
    reg_reader = csv.reader(f)
    print(dict_reader.fieldnames)
    print(dict_reader.restval)
    print(dict_reader.dialect)
    print(dict_reader.restkey)
    print(dict_reader.line_num)
    print(*[i for i in dict_reader])
    #print(*[i for i in reg_reader])
