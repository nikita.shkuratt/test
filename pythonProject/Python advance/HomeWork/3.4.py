# Для таблицы «материала» из дополнительного задания создайте пользовательскую агрегатную функцию,
# которая считает среднее значение весов всех материалов результирующей выборки и округляет данной значение до целого.

# Создайте таблицу «материалы» из следующих полей: идентификатор, вес, высота и доп.
# характеристики материала. Поле доп. характеристики материала должно хранить в себе массив,
# каждый элемент которого является кортежем из двух значений, первое – название
# характеристики, а второе – её значение.
import csv
with open('for_3.4.csv', 'w') as f:
    writer = csv.DictWriter(f, ['identifier', 'weight', 'height', 'additional characteristics of material'])
    writer.writeheader()
    writer.writerow({
        'identifier': 'brick',
        'weight': '1000kg',
        'height': '10cm from 1',
        'additional characteristics of material': ("Ukladochyi", "Kirpich — keramicheskij shtuchnoe isdelie")
    })
    writer.writerow({
        'identifier': 'plitka',
        'weight': '50kg',
        'height': '1cm from 1',
        'additional characteristics of material': [('Ukladocnyi'),
        ('Bolshoe znachenie imeet pravilnyi vybor instrumentow')]
    })
    writer.writerow({
        'identifier': 'cement',
        'weight': '200kg',
        'height': '50cm from 1',
        'additional characteristics of material': [('Stroitelnyi'),
        ('Iskustvennoe neogranichenoe gidravlicheskoe vagushee weshestwo')]
    })

def count_kg(file):
    with open(file, 'r') as f:
        dict_reader = csv.DictReader(f)
        result = ''
        temp = 0
        for i in dict_reader:
            result += i.get('weight')
            temp += 1
        result = (sum([int(i) for i in result.split('kg') if i.isdigit()]))
        return result // temp


print(f'средний вес всех материалов : {count_kg("for_3.4.csv")}')
