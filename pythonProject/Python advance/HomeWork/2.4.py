from urllib import request
import requests

# получаем содержимое страницы по domain- в качестве порта будет использован 80
# Для указания другого порта используем запись вида: http://example:81.com
response = request.urlopen('https://jsonplaceholder.typicode.com/')

# печатаем информацию об http(loyal)-ответе
print(response.status)
print(response.getcode())
print(response.msg)
print(response.reason)
# получаем заголовки в виде внутреннего объекта
print(response.headers)
# получаем словарь всех заголовков
print(response.getheaders())
# получение заголовка
print(response.headers.get('Content-Type'))
print(response.getheader('Content-Type'))

print(response.headers.get('Expires'))
print(response.getheader('Expires'))

res = requests.get('https://jsonplaceholder.typicode.com/')
print(res)
res1 = requests.put('https://jsonplaceholder.typicode.com/')
print(res1)

res2 = requests.post('https://jsonplaceholder.typicode.com/')
print(res2)
res3 = requests.head('https://jsonplaceholder.typicode.com/')
print(res3)
