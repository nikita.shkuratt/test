# Разработайте сокет сервер на основе библиотеки asyncio. Сокет сервер должен выполнять сложение двух чисел,
# как из предыдущего примера по многопоточности.
import cmd
import socket
import asyncio
import threading


# async def handle_client(client):
#     loop = asyncio.get_event_loop()
#     request = None
#     while request != 'quit':
#         request = (await loop.sock_recv(client, 1024)).decode('utf8')
#         response = str(request) + '\n'
#         await loop.sock_sendall(client, response.encode('utf8'))
#         if request == 'add':
#             request = (await loop.sock_recv(client, 1024)).decode('utf8')
#             response = sum(int(i) for i in request.split() if i.isdigit())
#             await loop.sock_sendall(client, str(response).encode('utf8'))
#     client.close()
#
# async def run_server():
#     server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#     server.bind(('localhost', 55555))
#     server.listen(8)
#
#     loop = asyncio.get_event_loop()
#
#     while True:
#         client, addr = await loop.sock_accept(server)
#         loop.create_task(handle_client(client))
#
# asyncio.run(run_server())

async def handle_client(reader, writer):
    request = None
    while request != 'quit':
        request = (await reader.read(1024)).decode('utf8')
        response = str(request) + '\n'
        writer.write(response.encode('utf8'))
        await writer.drain()
        if request == 'add':
            request = (await reader.read(1024)).decode('utf8')
            response = sum(int(i) for i in request.split() if i.isdigit())
            writer.write(str(response).encode('utf8'))
            await writer.drain()

    writer.close()

async def run_server():
    server = await asyncio.start_server(handle_client, 'localhost', 5555)
    async with server:
        await server.serve_forever()

asyncio.run(run_server())
