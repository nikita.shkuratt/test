# Создайте UDP сервер, который ожидает сообщения о новых устройствах в сети.
# Он принимает сообщения определенного формата, в котором будет идентификатор устройства
# и печатает новые подключения в консоль. Создайте UDP клиента,
# который будет отправлять уникальный идентификатор устройства на сервер, уведомляя о своем присутствии.
import socket
import socketserver

# sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# sock.bind(('localhost', 8888))
# while True:
#     print('Waiting to connect')
#     try:
#         result = sock.recv(1024)
#     except KeyboardInterrupt:
#         sock.close()
#         break
#     else:
#         print('Message: ', result.decode('utf-8'))

class EchoUDPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        data, sock = self.request
        print('Adress: ', self.client_address[0])
        sock.sendto(data, self.client_address)

with socketserver.UDPServer(('localhost', 8888), EchoUDPHandler) as server:
    server.serve_forever()

