# Создайте сопрограмму, которая получает контент с указанных ссылок и логирует ход выполнения в
# специальный файл используя стандартную библиотеку urllib, а затем проделайте то же самое с
# библиотекой aiohttp. Шаги, которые должны быть залогированы: начало запроса к адресу X, ответ для
# адреса X получен со статусом 200. Проверьте ход выполнения программы на >3 ресурсах и посмотрите
# последовательность записи логов в обоих вариантах и сравните результаты. Для двух видов задач
# используйте разные файла для логирования, чтобы сравнить полученный результат.