# Создайте три функции, одна из которых читает файл на диске с заданным именем и проверяет наличие строку “Wow! ”.
# В случае, если файла нет, то засыпает на 5 секунд, а затем снова продолжает поиск по файлу.
# В случае, если файл есть, то открывает его и ищет строку “Wow!”.
# При наличии данной строки закрывает файл и генерирует событие,
# а другая функция ожидает данное событие и в случае его возникновения выполняет удаление этого файла.
# В случае если строки «Wow!» не было найдено в файле, то засыпать на 5 секунд.
# Создайте файл руками и проверьте выполнение программы.
import asyncio
import os

async def read_file(file, future):
    while not os.path.exists(file):

        await asyncio.sleep(5)
        file = input('input correct path to file')

    with open(file, 'r') as f:
        for i in f:
            if i == 'wow!\n':
                print("Строка 'wow!' была найдена")
                break
            await asyncio.sleep(2)
    future.set_result(1)


async def delete_file(file, future):
    await future
    print('File was deleted')
    return os.remove(os.path.join(file))



file = 'foor5.2.txt'
event_loop = asyncio.get_event_loop()

fut = asyncio.Future()
tasks_list = [
event_loop.create_task(read_file(file, fut)),
event_loop.create_task(delete_file(file, fut))
]
tasks = asyncio.wait(tasks_list)
event_loop.run_until_complete(tasks)

event_loop.close()
