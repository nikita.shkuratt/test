# Для таблицы «материала» из дополнительного задания создайте пользовательскую функцию,
# которая принимает неограниченное количество полей и возвращает их конкатенацию.
import csv
def count_kg(file):
    with open(file, 'r') as f:
        reader = csv.reader(f)
        result = []
        for i in reader:
            if len(i) >= 1:
                result.append(i)
        return result

'''Это задание правильно сделано?'''
print(count_kg("for_3.4.csv"))