import socket
import subprocess

machine_id = subprocess.check_output('wmic csproduct get uuid').decode().split('\n')[1].strip()
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.sendto(machine_id.encode('utf8'), ('localhost', 8888))
