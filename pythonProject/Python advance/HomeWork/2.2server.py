# Создайте UNIX сокет, который принимает сообщение с двумя числами, разделенными запятой.
# Сервер должен конвертировать строковое сообщения в два числа и вычислять его сумму.
# После успешного вычисления возвращать ответ клиенту
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('localhost', 8888))

while True:
    try:
        print("server is on")
        data, addr = sock.recvfrom(1024)
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        temp = 0
        for i in data.decode('utf8').split(','):
            temp += int(i)
        sock.sendto(str(temp).encode('utf8'), addr)



