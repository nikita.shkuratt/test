# Используя модуль sqlite3 и модуль smtplib, реализуйте реальное добавление пользователей в
# базу. Должны быть реализовать следующие функции и классы:
# - класс пользователя, содержащий в себе следующие методы: get_full_name (ФИО с разделением
# через пробел: «Петров Игорь Сергеевич»), get_short_name (ФИО формата: «Петров И. С.»),
# get_age (возвращает возраст пользователя, используя поле birthday типа datetime.date); метод
# __str__ (возвращает ФИО и дату рождения).
# - функция регистрации нового пользователя (принимаем экземпляр нового пользователя и
# отправляем Email на почту пользователя с благодарственным письмом)
import smtplib
import sqlite3
import datetime


class User:

    def __init__(self, name: str, surname: str, midl_name: str, age: tuple):
        self.name = name
        self.surname = surname
        self.midl_name = midl_name
        self.age = age

    def get_full_name(self):
        return f'{self.name}, {self.surname}, {self.midl_name}'

    def get_short_name(self):
        return f'{self.surname} {self.name[0]}. {self.midl_name[0]}.'

    def get_age(self):
        return ', '.join([str(i) for i in self.age])

    def __repr__(self):
        return self.get_full_name() + " " + self.get_age()


u_data = User('Nikita', "Familija", 'Otchestvo', (11, 8, 1998))
print(u_data.get_full_name())
print(u_data.get_age())
print(u_data.get_short_name())
print(u_data)
print('------------')

message = ''

with sqlite3.connect('for7.3.db') as db:
    c = db.cursor()
    create_tabl = '''CREATE TABLE IF NOT EXISTS userdata
                (id INTEGER PRIMARY KEY,
                name TEXT,
                surname TEXT,
                midl_name TEXT,
                age TEXT
                email TEXT NOT NULL UNIQUE)
                '''
    #c.execute(create_tabl)

    data_for_insert = ('''INSERT INTO userdata (name, surname, midl_name, age)
                        VALUES (?, ?, ?, ?)''')
    data_tuple = (u_data.name, u_data.surname, u_data.midl_name, u_data.get_age())

    #c.execute(data_for_insert, data_tuple)
    for i in c.execute('SELECT * FROM userdata'):
        message += str(i)

ask_to_send = input('Do you want send email? y/n\n>>')
if ask_to_send == 'y':
    sender = input('Sender\n>>')
    password = input('Password\n>>')
    reciver = input('Reciver\n>>')
    try:
        smtpObj = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        smtpObj.login(sender, password)
        smtpObj.sendmail(sender, reciver, message)
        print('Successfully sent email')
    except Exception as e:
        print('Fail to sent:', e)