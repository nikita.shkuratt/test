#Создайте простые словари и сконвертируйте их в JSON.
# Сохраните JSON в файл и попробуйте загрузить данные из файла.
import json

test_dict = {'1': 11,
             '2': 22,
             '3': 33,
             '4': 44,
             '56': [55, 66]}

# with open('for_3.1.json', 'w') as f:
#     json.dump(test_dict, f)

with open('for_3.1.json', 'r') as f:
    print(json.load(f))