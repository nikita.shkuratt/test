# Создать агрегатные функции для подсчёта общего количества расходов и расходов за месяц.
# Обеспечить соответствующий интерфейс для пользователя
import sqlite3

def count_expenses(num=False):

    with sqlite3.connect('db_for4.1.db') as db:
        cursor = db.cursor()
        result = 0
        for tup_le in (cursor.execute('select sum from expenses').fetchmany(num)):
            for integer in tup_le:
                result += integer
    return result


input_for_count_expenses = (input("Ведите количество в днях, по умолчанию считаем расходы за всё время\n"))

print(count_expenses(int(input_for_count_expenses) if input_for_count_expenses.isdigit() else False))