import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.sendto(b'3, 22, 6', ('localhost', 8888))

while True:
    try:
        print("client is on")
        result = sock.recv(1024)
    except KeyboardInterrupt:
        sock.close()
        break
    else:
        print('Result :', result.decode('utf8'))
