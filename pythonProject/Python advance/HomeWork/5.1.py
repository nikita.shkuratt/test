# Создайте функцию по вычислению факториала числа. Запустите несколько задач,
# используя ThreadPoolExecutor и замерьте скорость их выполнения,
# а затем замерьте скорость вычисления, используя тот же самый набор задач на ProcessPoolExecutor.
# В качестве примеров, используйте крайние значения, начиная от минимальных и заканчивая максимально возможными,
# чтобы увидеть прирост или потерю производительности.
import functools
import time
import threading
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor


def factorial(n=2):
    result = []
    temp = n
    for i in range(1, n):
        result.append(temp)
        temp -= 1
    r_for_res = functools.reduce(lambda x, y: x * y, result)
    return (f'factorial {n}  ')


def run_by_executor(executor_class, max_workers=2):
    executor = executor_class(max_workers)
    started = time.time()
    #future1 = executor.submit(factorial, n=50000)
    future2 = executor.submit(factorial, n=50000)
    result = future2.result()
    print(f'Result: {result}, Name: {executor_class.__name__} Time: {time.time() - started}')

task = threading.Thread(target=factorial, kwargs={"n": 10000})
time_start = time.time()
task.start()
task.join()
print(f' Result 1. Time : {time.time() - time_start}')

task = threading.Thread(target=factorial, kwargs={"n": 50000})
time_start = time.time()
task.start()
task.join()
print(f' Result 2. Time : {time.time() - time_start}')

time_start = time.time()
factorial(50000)
print(f' Result 2 . Time : {time.time() - time_start}')

#run_by_executor(ThreadPoolExecutor)


started = time.time()
executor = ThreadPoolExecutor(max_workers=4)
for exec in executor.map(factorial(50000)):
    print(exec)
print(f'Time in map {time.time() - started}')

started = time.time()
executor = ThreadPoolExecutor(max_workers=4)
result = executor.submit(factorial, n=50000).result()
print(f'Time in sumbit {time.time() - started}')
# print("---------------------------------------------------------------------------------------------------------")
if __name__ == '__main__':
    started = time.time()
    executor = ProcessPoolExecutor(max_workers=4)
    print(executor.submit(factorial, n=50000).result())
    print(f'Name {executor.__class__} Time in submit {time.time() - started}')

    started = time.time()
    executor = ProcessPoolExecutor(max_workers=4)
    for exec in executor.map(factorial(50000)):
        print(exec)
    print(f'Name {executor.__class__}Time in map {time.time() - started}')


