'''Определяет является ли число простым'''
# def is_simple_num(x):
#     divisor = 2
#     while divisor < x:
#         if x % divisor == 0:
#             return False
#         divisor += 1
#     return True
#
# print([i for i in range(3, 100) if is_simple_num(i)==True])
# ----------------------------------------------------------------------------------------------------------------------

'''Раскладывает число на множители'''
# def factorize_num(x):
#     divisor = 2
#     while x > 1:
#         if x % divisor == 0:
#             print(divisor)
#             x //= divisor
#         else:
#             divisor += 1
#
# factorize_num(130)
# ----------------------------------------------------------------------------------------------------------------------

'''Осуществляет поиск числа x в листе а'''
# def array_search(a:list, n:int, x:int):
#     for k in range(n):
#         if a[k]==x:
#             return k
#     return -1
#
# def test_array_search():
#     a1 = [1, 2, 3, 4, 5]
#     m = array_search(a1, 5, 6)
#     if m == -1:
#         print('test1-ok')
#     else:
#         print('test1-fail')
#     a2 = [-1, -2, -3, -4, -5]
#     m = array_search(a2, 5, -3)
#     if m == 2:
#         print('test2-ok')
#     else:
#         print('test2-fail')
#     a3 = [30, 10, 30, 40, 10]
#     m = array_search(a3, 5, 10)
#     if m == 1:
#         print('test3-ok')
#     else:
#         print('test3-fail')
#
# test_array_search()
# ----------------------------------------------------------------------------------------------------------------------

'''Обращение массива задом наперед'''
# def invert_array(lst=[1, 3, 4, 2, 10], n=5):
#     temp_lst = []
#     for i in range(1, n+1):
#         temp_lst += [lst[-i]]
#     lst = temp_lst
#     return lst
# def invert_array(lst=[1, 3, 4, 2, 10], n=5):
#     for k in range(n//2):
#         lst[k], lst[n-1-k] = lst[n-1-k], lst[k]
#     return lst
# print(invert_array())
# ----------------------------------------------------------------------------------------------------------------------

"""Реализации алгоритмов/Решето Эратосфена"""
# def resheto_eratosfena(n):
#     A = list([True] * n)
#     A[0:1] = False, False
#     for i in range(2, n):
#         if A[i]:
#             for k in range(2*i, len(A), i):
#                 A[k]= False
#     return A
#
# for k in range(25):
#     print(k, '-', "prostoe" if resheto_eratosfena(25)[k] else 'sostavnoe')

'''Полностью построено на генераторах списков'''
# n = 25 #int(input())
# b = [list(range(2 * j, n + 1, j)) for j in range(2, n//2)] # Разбиение на части генератора
# print([x for x in range(3, n+1) if x not in [i for sub in b for i in sub]]) # Разбитие генератора на цикл фор

# for sub in b:
#     for i in sub:
#         print(i, end=' ')
# ----------------------------------------------------------------------------------------------------------------------
# a = [2, 1, 5, 3, 4]
'''Сортировка списка А вставками'''
# def insert_sort(A):
#     for top in range(1, len(A)):
#         k = top
#         while k > 0 and A[k-1] > A[k]: # 0 1
#             A[k], A[k-1] = A[k-1], A[k]
#             k -= 1
#     return A
#
# #print(insert_sort(a))
'''Сортировка списка А выбором'''
# def choise_sort(A):
#     for pos in range(0, len(A)-1):
#         for k in range(pos+1, len(A)):
#             if A[k] < A[pos]: #if A[pos] > A[k]
#                 A[k], A[pos] = A[pos], A[k]
#     return A
# #print(choise_sort(a))
#
'''Сортировка списка А методом пузыря'''
# def bubble_sort(A):
#     for bypass in range(1, len(A)):
#         for k in range(0, len(A)-1):
#             if A[k] > A[k+1]:
#                 A[k], A[k+1] = A[k+1], A[k]
#     return A
# #print(bubble_sort(a))

import graphics as gr

window = gr.GraphWin('Russian game', 300, 300)

def fractal_rectangle(a, b, c, d, deep=10):
    if deep < 1:
        return
    for m, n in (a, b), (b, c), (c, d), (d, a):
        gr.Line(gr.Point(*m), gr.Point(*n)).draw(window)


my_rectangle = gr.Rectangle(gr.Point(2, 4), gr.Point(4, 8))
my_rectangle.draw(window)
