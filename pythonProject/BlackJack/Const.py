SUITS = ['\u2665', '\u2666', '\u2663', '\u2660']
RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace']
PRINTED = {
    'Ace': 'A', 'King': 'K', 'Queen': 'Q', 'Jack': 'J',
    "10": "10", "9": '9', "8": '8', "7": '7', "6": '6',
    "5": '5', "4": '4', "3": '3', "2": '2'}

MASSAGES = {
    'ask_start': 'Want to play?(y/n)',
    'ask_card': 'Want new card?(y/n)',
    'eq': '{player} has {points} points, it equal with dealer points\n {player} bid will be back',
    'win': 'Player {name} win with cards {cards}',
    'lose': '{} player lose',
    'rerun': 'Do you want to play again? y/n'
}
NAMES = ['John', "Jack", 'Ivan', "Vasya"]