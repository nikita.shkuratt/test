class Point:
    __x = 0
    __y = 0
    def __init__(self,x, y):# Konstruktor
        self.__x = x
        self.__y = y
    def getX(self):
        return self.__x
    def getY(self):
        return self.__y
    def setX(self, x):
        self.__x = x
    def setY(self, y):
        self.__y = y
    def __test(self): #PRIVATE
        print("Privat metod")
    def runPrivate(self): #PUBLIC
        self.__test()
p = Point(10, 15)#Ekzemplar klassa
#print(p.__x) ошибка потому что закрытые свойства
print(p.getX())
print(p.getY())
p.setX(20)
p.setY(8)
print(p.getX())
print(p.getY())
#print(p.__test) ошибка потому что закрытые свойства
p.runPrivate()