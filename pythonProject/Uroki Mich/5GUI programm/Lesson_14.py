from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))

root = Tk()
setwindow(root)

photo = PhotoImage(file="1-7.png")
bgphoto = photo.zoom(2,2)
bg = Label(root,image=bgphoto)
bg.place(x=0,y=0,relheight=1,relwidth=1)

buttonimage = photo.subsample(2, 2)
button = Button(root,image=buttonimage)
button.pack()

root.mainloop()# бесконечный цикл для программы на открытие


#photo = PhotoImage(file=input("Введите название файла:"))
#input2 = float(input("Укажите масштаб изображенияX: "))
#input1 = float(input("Укажите масштаб изображенияY: "))
#bgphoto = photo.zoom(int(input2), int(input1))