from random import *
a = randint(1,100)
from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))

root = Tk()
setwindow(root)
def handlerclick1(args):
    a = randint(1, 100)
    print(a)
def handlerclick2():
    print("Najata knopka")
def handlerclick3(event):
    print("Kliknnuli pravoj knopkoy myshi po knopke")
    print(event.widget.cget('text'))
def handlerroot(event):
    print(event)
    print("Сработало событие")

button1 = Button(root,text="Cgeneryrovat sluch 4islo",font="Tahoma 20",command=lambda: handlerclick1("kNopka1"))
button1.pack()

#button2 = Button(root,text="Knopka2",font="Tahoma 20",command=handlerclick2)
#button2.pack()
#button2.bind("<Button-3>",handlerclick3)

#button3 = Button(root,text="Knopka3",font="Tahoma 20")
#button3.bind("<Button-3>",handlerclick3)
#button3.pack()

#root.bind('a',handlerroot)

root.mainloop()# бесконечный цикл для программы на открытие

#1) Сделайте кнопку с текстом «Сгенерировать случайное число».
#2) При клике по кнопке под ней должно появляться случайное целое число от 1 до 100.
#Примечание: для вывода сгенерированного числа используйте Label.