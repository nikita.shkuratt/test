from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))
root = Tk()
setwindow(root)

label = Label(root,text="Favorite color",font="Tahoma 20")
choise = StringVar()
r1 = Radiobutton(root,text="Red butt",font="Tahoma 20",variable=choise,value="red")
r2 = Radiobutton(root,text="Green butt",font="Tahoma 20",variable=choise,value="green")
r3 = Radiobutton(root,text="Blue butt",font="Tahoma 20",variable=choise,value="blue")
choise.set("red")

label.pack()
r1.pack()
r2.pack()
r3.pack()

root.mainloop()