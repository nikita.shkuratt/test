from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))

root = Tk()
setwindow(root)

label1 = Label(root,text="Metka1",font="Tahoma 20",bg="red",width=10,height=2)
label2 = Label(root,text="Metka2",font="Tahoma 20",bg="green",width=10,height=2)
label3 = Label(root,text="Metka3",font="Tahoma 20",bg="blue",width=10,height=2)
label4 = Label(root,text="Metka4",font="Tahoma 20",bg="red",width=10,height=2)
label5 = Label(root,text="Metka5",font="Tahoma 20",bg="red",width=10,height=2)
label1.pack(side='top',anchor="n")
label2.pack(side='left')
label3.pack(side='right')
label4.pack(side='bottom')
label5.pack(side='bottom',expand=True)



root.mainloop()# бесконечный цикл для программы на открытие
#1) Сделайте 4 элемента Label, задав у них одинаковые параметры width и height.
#2) Выведите первый Label вверху по центру, второй и третий слева и справа соответственно под первым элементом, а четвёртый по центру снизу.