from tkinter import *


def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()

    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))


root = Tk()
setwindow(root)

entry = Entry(root,font="Tahoma 20",bg="Yellow",fg="Green",bd=4)
entry1 = Entry(root,font="Tahoma 20",bg="Yellow",fg="Green",bd=4,show="*")

entry.insert(END,"Hello")
entry.insert(END,"HASDSAD")

print(entry1.cget("font"))
print(entry1["font"])

entry1['font'] = 'Tahoma 40'
entry1['show'] = 'a'




entry.pack()
entry1.pack()

root.mainloop()  # бесконечный цикл для программы на открытие