from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))

root = Tk()
setwindow(root)

photo = PhotoImage(file=input("Введите название файла:"))
input2 = float(input("Укажите масштаб изображенияX: "))
input1 = float(input("Укажите масштаб изображенияY: "))
if input1 and input2 > 1:
    bgphoto = photo.zoom(int(input2), int(input1))
if input1 and input2 < 1:
    bgphoto = photo.subsample(int(1 / input2), int(1 / input1))
bg = Label(root,image=bgphoto)
bg.place(x=0,y=0,relheight=1,relwidth=1)

buttonimage = photo.subsample(4, 4)
button = Button(root,image=buttonimage)
button.pack()

root.mainloop()# бесконечный цикл для программы на открытие

#1) Попросите пользователя ввести путь к картинке.
#2) Следующим шагом попросите его указать, с каким масштабом выводить изображение.
#3) Выведите его в окно программы с указанным пользователем масштабированием, используя Label.
#Примечание: нужно учесть, что, если пользователь вводит значения меньше 1, значит, он хочет его сжать, и для этого потребуется функция subsample, а также перевод, например, 0.2 в число 5 (1 / 0.2).