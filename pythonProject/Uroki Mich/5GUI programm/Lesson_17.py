from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))
def handlerbutton(event=False):
    global en1
    global en2
    global display
    try:
        r = str(float(en1.get()) + float(en2.get()))
        result.config(text="Summa 4isel rovna:" + r)
    except ValueError:
        if not en1.get() or not en2.get():
            result.config(text="Вы ничего не ввели")
        else:
            result.config(text="Вы ввели не числа")
root = Tk()
setwindow(root)


#Ostanovilsa na 5:36
header = Label(root,text="Суммирование",font="Tahoma 20")
en1 = Entry(root,font="Tahoma 20")
en2 = Entry(root,font="Tahoma 20")

button = Button(root,text="Summa",font="Tahoma 20",command=handlerbutton)
display = Label(root, font="Tahoma 20")

en1.bind("<Return>",handlerbutton)
en2.bind("<Return>",handlerbutton)


header.place(relx = 0.5,rely= 0.01,anchor="n")
en1.place(relx = 0.5,rely= 0.1,anchor="n")
en2.place(relx = 0.5,rely= 0.2,anchor="n")
button.place(relx = 0.5,rely= 0.3,anchor="n")
display.place(relx = 0.5, rely= 0.4, anchor="n")




root.mainloop()# бесконечный цикл для программы на открытие