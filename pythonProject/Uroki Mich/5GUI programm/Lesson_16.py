from tkinter import *
def setwindow(root):
    root.title("Окно программы")
    root.resizable(False, False)  # Возможность разширения

    w = 800
    h = 600
    ws = root.winfo_screenwidth()
    wh = root.winfo_screenheight()
    x = int(ws / 2 - w / 2)
    y = int(wh / 2 - h / 2)
    print(x, y)

    # root.geometry("800x600+560+240")
    root.geometry("{0}x{1}+{2}+{3}".format(w, h, x, y))

def handlerenter(event):
    event.widget.config(bg='red')

def handlerleav(event):
    event.widget.config(bg='green')
root = Tk()
setwindow(root)

button1 = Button(root,text="Knopka1",font="Tahoma 20",bg='yellow')
button2 = Button(root,text="Knopka2",font="Tahoma 20",bg='yellow')


button1.bind('<Enter>',handlerenter)
button1.bind('<Leave>',handlerleav)

button2.bind('<Enter>',handlerenter)
button2.bind('<Leave>',handlerleav)

button1.pack()
button2.pack()


root.mainloop()# бесконечный цикл для программы на открытие