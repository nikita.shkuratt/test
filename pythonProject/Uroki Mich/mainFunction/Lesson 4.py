set1 = {1,2,3}
set2 = {1,2,3,4}
print(len(set1))

set1.add(10)
print(set1)
set1.remove(10)
print(set1)

#set1.remove(5) #нет 5 поэтому ошибка
set1.discard(5)# без проверки
print(set1)
print("------------------")
set1 = {2,3,4}
set2 = {1,2,3}

print(set1 == set2)
print(set1 <= set2)
print(set1 >= set2)
print(set1.union(set2))# объединение 2 множеств
print(set1.intersection(set2)) # Выводит числа из 2 множеств
print(set1.difference(set2)) # вычитание из вервого множества 2ым
print("------------------")
print("------------------")
print("------------------")
set1 = {1,3,2,2,4,8,4,2,7,10}
set2 = {1,2,5,8,3,2,1,9,10,10}
print(set1.union(set2))
#1) Создайте 2 множества, состоящие из 10 случайных целых чисел от 1 до 10 (включая 1 и 10).
#2) Выведите 3 множества: объединением этих двух множеств, их разницей и их пересечением.