def isfloat(x):
    try:
        x = float(x)
        return True
    except ValueError:
        return False
    print(isfloat(absnnnn))
print("----------------")
s1 = 'str_1'
s2 = 'str_2'
print(len(s1))
print(s1[1])
print(s1[-1])
print(s1[-2])
print(s1[0:3])

s1 = 'abc\\nxyz'
s2 = r'abc\nxyz' #игнор переноса на новую строку \n = \\
print(s1)
print(s2)

s1 = "hello"
print(s1 * 3)
print(s1.find("l",3))

print("593".isdigit()) #проверяет,состоит ли строка из букв
print(s1.isalpha()) #проверяет,состоит ли строка из чисел
print(s1.lower())
print(s1.upper())

print("----------------")

print(ord("a")) #получаем номер буквы
print(chr(97)) # получаем букву с номера 97

s1 = "       hiSS         "
print(s1.strip()) # функция стрип убирает пробелы
s1 = "Здраствуйте, {0}. Вам {1} лет!" #Шаблое
print(s1.format("Alex", 30)) # Значения для шаблона
print("----------------")
print("----------------")

data = ("Alex", 30)
s1 = "Здраствуйте, {0[0]}. Вам {0[1]} лет!" #Шаблое с картежом !!!!!!!! Важно
print(s1.format(data))
print("----------------")
print("----------------")

s1 = "int: {0:d}; bin: {0:b} " #перевод в десятичную и двоичною
print(s1.format(5))

s1 = " Round (150 / 47) {0:.2}"
print(s1.format(150/47))
