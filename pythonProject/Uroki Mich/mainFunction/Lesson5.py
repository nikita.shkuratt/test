d = {'name': ' Alex', 'age': 35}
print(d)
d.setdefault('isWorking', True)
print(d)
print(d.get('age'))
d.pop('name')   # удаление ключ значение
print(d)

keys = list(d.keys())
print(list(d.keys()))
print(keys[0])

values = list(d.values())
print(values)
print(values[0])

d['age'] = 40
d['isMale'] = True
print(d)

d.clear()
print(d)

hbl = {'Здраствуй': 'Hello', 'Пока' : 'Bye', 'Урок' : 'Lesson'}
key = list(hbl.keys())
print(key)
print("----------------------------------------------------------------------------------------")
import random

dictionary = {'Hello': 'Здравствуй',

             'Bye': 'Пока',

             'Lesson': 'Урок'}

done = False

while not done:

   eng, rus = random.choice(list(dictionary.items()))

   while True:

       answer = input('Переведите слово "' + rus + '":').lower()

       if answer == 'quit':

           done = True

           break

       if answer == 'show':

           print(dictionary)

       elif answer == eng.lower():

           break

       else:

           print('Неверно')