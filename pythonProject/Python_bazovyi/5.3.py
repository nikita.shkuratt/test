class Reversed:
    def __init__(self, max):
        self.max = max
        pass

    def __iter__(self):
        self.count = 0
        self.num = (1, 1)
        return self

    def __next__(self):
        cur_num, last_num = self.num

        last_num = last_num - cur_num

        self.num = cur_num, last_num
        self.count += 1
        if self.count > self.max:
            raise StopIteration
        return last_num


rev = Reversed(5)
for number in rev:
    print(number)
