# gen = [1, 2, 5, 8, 10, 55]
# def rev_generator(count=len(gen)):
#     s = 0
#     for i in range(0,count):
#         s -= 1
#         gen[i] = gen[s]
#     yield gen
#
#
# for gen in rev_generator():
#     print(gen)
g = [1, 2, 5, 8, 10, 55]
def reverse_generator(lst):
    yield lst[::-1]


rev = reverse_generator(g)

for gen in rev:
    print(gen)