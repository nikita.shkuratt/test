class Person:
    def __init__(self, name, age, height, weight):
        self.n = name
        self.a = age
        self.h = height
        self.w = weight


class Worker(Person):
    def __init__(self, name, age, height, weight, specalization):
        super().__init__(name, age, height, weight)
        self.speca = specalization


class Salary(Worker):
    def __init__(self, name, age, height, weight, specalization, salary, hours):
        super().__init__(name, age, height, weight, specalization)
        self.salary = salary
        self.hours = hours

    def info(self):
        return f"{self.n}, age {self.a},height{self.h}, weight{self.w}, spez{self.speca}" \
               f"slary {self.salary} hours {self.hours}"

    def get_salary(self):
        return str(self.salary) + "$"

    def set_salary(self, x):
        self.salary = x

    def set_hours(self, h):
        self.hours = h

    def get_hours(self):
        if self.hours > 176:
            return 'Your salary this month' + str((1.2 * (self.hours - 176)
                                                   + self.salary)) + "$. Thanks for your job"


worker1 = Salary("John", 23, 180, 80, "QA", 300, 150)
print(worker1.info())
worker1.set_salary(int(input("Set salary")))
print(worker1.get_salary())
worker1.set_hours(int(input("Set hours")))
print(worker1.get_salary())
print(worker1.get_hours())
