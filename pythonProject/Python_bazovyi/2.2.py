import pygame

pygame.init()

W = 600
H = 400

sc = pygame.display.set_mode((W, H))
pygame.display.set_caption("События от мыши")

WHITE = (255, 255, 255)
RED = (255, 0, 0)

FPS = 60  # число кадров в секунду
clock = pygame.time.Clock()


class Rectangle:
    @staticmethod
    def click_button():
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                print("Najata knopka:", event.button)

    @staticmethod
    def draw_rec():
        draw = False
        sp = None

        sc.fill(WHITE)
        pygame.display.update()

        while 1:
            Rectangle.click_button()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    exit()
                elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    draw = True
                    sp = event.pos
                elif event.type == pygame.MOUSEMOTION:
                    if draw:
                        pos = event.pos

                        width = pos[0] - sp[0]
                        height = pos[1] - sp[1]

                        sc.fill(WHITE)
                        pygame.draw.rect(sc, RED, pygame.Rect(sp[0], sp[1], width, height))
                        pygame.display.update()
                elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                    draw = False


Rectangle.draw_rec()
