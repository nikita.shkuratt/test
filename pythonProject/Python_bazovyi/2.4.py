import random


class Vehicle:
    def __init__(self, power, num_seats, speed, capacity):
        self.p = power
        self.n_s = num_seats
        self.s = speed
        self.c = capacity
        print("Vechicle stats")

    def sedan(self):
        return f" Here sedan and it's stats: {self.p}, {self.n_s}, {self.s}, {self.c}"

    def suv(self):
        return f" Here sedan and it's stats: {self.p}, {self.n_s}, {self.s}, {self.c}"

    def super_car(self):
        return f" Here sedan and it's stats: {self.p}, {self.n_s}, {self.s}, {self.c}"

    def truck(self):
        return f" Here sedan and it's stats: {self.p}, {self.n_s}, {self.s}, {self.c}"


sedan = Vehicle(300, 5, 250, "300 l")
print(sedan.sedan())
suv = Vehicle(200, random.randint(5, 7), 200, "450 l")
print(suv.suv())
super_car = Vehicle(800, 2, 350, "150 l")
print(super_car.super_car())
truck = Vehicle(600, 2, 90, "30k T")
print(truck.truck())
