def simpl_num(n):
    i = 3
    while i <= n:
        f = True
        j = 2
        while f and j < i:
            if not i % j:
                f = False
            j += 1
        if f:
            yield i
        i += 1


sim = simpl_num(10000000)
for i in range(int(input("ведите колл элементов"))):
    print(next(sim), end=' ')


# def simpl_num():
#     for i in range(3, 250):
#         for j in range(2, i):
#             if i % j == 0:
#                 break
#         else:
#             yield i
#
#
# s = simpl_num()
# for i in range(5):
#     print(next(s))
