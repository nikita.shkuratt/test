import random

secret = random.randint(1, 100)
guess = 0
while guess != secret:
    guess = int(input("Choice  number"))
    if guess < secret:
        print("Number low, try again")
    elif guess > secret:
        print("Number higher, try again")
    elif guess == secret:
        print("Congratulation comrade, your choice correct ")