class Base:
    def method(self):
        hello = "Hello from Base"
        print(hello)

class Child(Base):
    def method(self):
        hello = "Hello from Child"
        print(hello)


Child.method(Base)
Base.method(Base)
