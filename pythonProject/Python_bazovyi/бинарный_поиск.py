def binary_search(lis, item):
    low = 0
    high = len(lis) - 1
    proverka = 0
    while low <= high:
        proverka += 1
        mid = int((low + high) / 2) # c помощью /2 получаем бинарный поиск
        guess = lis[mid]
        if guess == item:
            print(mid, "koll:", proverka)
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return None


my_list = list(range(256))

print(binary_search(my_list, 130))
print(binary_search(my_list, -1))