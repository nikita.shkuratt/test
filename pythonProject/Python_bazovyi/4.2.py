class Calk:
    def __init__(self, x, y, arg):
        self.x = x
        self.y = y
        self.arg = arg

    def calk_operation(self):
        if self.arg == "+":
            return self.x + self.y
        elif self.arg == "-":
            return self.x - self.y
        elif self.arg == "*":
            return self.x * self.y
        elif self.arg == "/":
            try:
                return self.x / self.y
            except:
                print("Деление на ноль запрещенно")
                return 0
        elif self.arg == "**":
            try:
                return self.x ** self.y
            except:
                return 0, "ведите коректно"

while True:
    try:
        calk = Calk(int(input("Первое число")), int(input("Второе число")), "/")
        print(calk.calk_operation())
    except:
        print("Вы не ввели число")

