#Ознакомьтесь при помощи документации с классами OrderedDict,
# defaultdict и ChainMap модуля collections.
from collections import OrderedDict

mydict = OrderedDict()
mydict['z'] = 'первый'
mydict['a'] = 'последний'

mydict['z'] = 'первый всё ещё'

print(mydict)

