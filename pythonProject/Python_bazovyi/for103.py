def is_prime(num):
    prime = num > 1 and (num % 2 != 0 or num == 2) and (num % 3 != 0 or num == 3)

    return prime

#print(*[ i for i in range(101) if is_prime(i)])