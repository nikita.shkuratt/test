"""Задание 3

Опишите класс сотрудника, который включает в себя такие поля, как имя, фамилия, отдел и год поступления на работу.
Конструктор должен генерировать исключение, если заданы неправильные данные. Введите список работников с клавиатуры.
Выведите всех сотрудников, которые были приняты после заданного года."""


class Employee:
    employee_list = []

    def __init__(self, name, surname, departament, jyear):
        self.name = name
        self.surname = surname
        self.departament = departament
        self.jyear = jyear

    def add_employee(self):
        self.employee_list.append([name, surname, departament, jyear])

    def get_employee(self):
        return Employee.employee_list

    def check_employee(self, a):
        # a - временная переменная (год), по которому будем искать :)
        new_list = []
        self.a = a

        for i in range(len(Employee.employee_list)):
            if a in Employee.employee_list[i]:
                new_list.append(Employee.employee_list[i])
        return new_list


z = 0
while z != 3:
    z = int(input("""Введите требуемую операцию\n
        1. Добавить нового сотрудника
        2. Вывести сотрудников по году вступления
        3. Выход
            """))
    if z == 1:
        try:
            name = str(input("Введите имя сотрудника:\n"))
            surname = str(input("Введите фамилию сотрудника:\n"))
            departament = str(input("Введите название департамента:\n"))
            jyear = int(input("Введите год поступления:\n"))

            e = Employee(name, surname, departament, jyear)
            e.add_employee()
        except:
            print("Ошибка ввода данных!")

    elif z == 2:
        a = int(input("Введите год:\n"))
        print(e.check_employee(a))

    elif z == 3:
        # вывод всех сотрудников (для теста)
        print(e.get_employee())
        break

    else:
        print('Некорректный ввод')
