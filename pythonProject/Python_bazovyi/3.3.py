# Используя ссылки в конце данного урока, ознакомьтесь с таким средством инкапсуляции как свойства.
# Ознакомьтесь с декоратором property в Python. Создайте класс, описывающий температуру и позволяющий
# задавать и получать температуру по шкале Цельсия и Фаренгейта, причём данные могут быть заданы в одной шкале,
# а получены в другой.
class Temperature:
    def __init__(self, celsius, fahrenheit):
        self.cel = celsius
        self.fahr = fahrenheit

    def set_cel_and_fahr(self, x):
        self.cel, self.fahr = x, x

    def get_temp(self):
        return f"Tempeerature in celsius:{self.cel}, in fahrenheit:{self.fahr * 1.8 + 32}"


temp = Temperature(None, None)
temp.set_cel_and_fahr(-169.53)
print(temp.get_temp())
