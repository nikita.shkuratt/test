class Review:
    def __init__(self, person, dina, cenka):
        self.person = person
        self.dlina = dina
        self.ocenka = cenka

    def __repr__(self):
        return f" Imya komentatora - {self.person}, dlina kommentarija - {self.dlina}, ocenka ot polzovatela - " \
               f"{self.ocenka}"


rev = Review("Anjei", "100 simvolow", "5")
print(rev)
print("_____________")


class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre

    def info(self):
        return self.author, self.name, self.year, self.genre

    def __repr__(self):
        return f" Author {self.author}, Name author {self.name}, Year {self.year}, Genre {self.genre}"

    @staticmethod
    def revv():
        return rev

    @staticmethod
    def coment():
        return Review("Anjei", "100 simvolow", "5")


book = Book('Josef', 'Atlant', 2003, 'fantasy')
print(book, ". Dannye komentarija :", Book.revv(), sep='')
print(book, ". Dannye komentarija :", Book.coment(), sep='')
