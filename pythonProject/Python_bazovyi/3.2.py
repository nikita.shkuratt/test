class English:
    @staticmethod
    def greeting():
        return "Hello in english "


class Spanish:
    @staticmethod
    def greeting():
        return "Hola en español"


eng = English.greeting()
sp = Spanish.greeting()


def hello_friend():
    return eng, sp


print(hello_friend())
