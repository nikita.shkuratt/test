# Создайте словарь с ключами-строками и значениями-числами.
# Создайте функцию, которая принимает произвольное количество именованных параметров.
# Вызовите её с созданным словарём и явно указывая параметры.
st = {'one': 1, 'two': 2, 'tree': 3, 'four': 4}


# def my_func(*args):
#     print(args)
#
#
# my_func(st, st.keys(), st.values())


def func(**kwargs):
    for key, value in kwargs.items():
        print("{} full link is {}".format(key, value))


func(st="sa", mail="msaf.comm")