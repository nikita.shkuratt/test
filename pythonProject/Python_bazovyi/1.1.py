class Book:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre

    def info(self):
        return self.author, self.name, self.year, self.genre

    def __repr__(self):
        return f" Author {self.author}, Name author {self.name}, Year {self.year},Genre {self.genre}"

    def __str__(self):
        return f" Author {self.author}, Name author {self.name}, Year {self.year},Genre {self.genre}"


class SecondBook:
    def __init__(self, author, name, year, genre):
        self.author = author
        self.name = name
        self.year = year
        self.genre = genre

    def info(self):
        return self.author, self.name, self.year, self.genre

    def __repr__(self):
        return f" Author {self.author}, Name author {self.name}, Year {self.year},Genre {self.genre}"

    def __str__(self):
        return f" Author {self.author}, Name author {self.name}, Year {self.year},Genre {self.genre}"


book1 = Book('Josef', 'Atlant', 2003, 'fantasy')
print(book1.info())
second_book = SecondBook('Josef', 'Atlant', 2003, 'fantasy')
print(book1 == second_book)
print(second_book.__repr__())
print(second_book.__repr__() == book1.__repr__())
print(second_book.__repr__() == book1.__str__())
print(second_book.info() == book1.info())
