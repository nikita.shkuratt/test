# Vyvod, sravnenie
# class Dog:
#     stomach_full_percentage = 20
#     def is_hungry(self):
#         if(self.stomach_full_percentage < 30):
#             return True
#         else:
#             return False
#
# snoopy = Dog()
# print(snoopy.is_hungry())




# class Dog:
#     def __init__(self, data):
#         self.name = data
#     def __str__(self):
#         return 'Dog:' + self.name
#     def __repr__(self):
#         return self.name
# dog = Dog('Snoopy')
# print(Dog.__repr__(dog))
# print(dog)

print("!!!!!!!!!!!!!!!!!!!")
# class BasicCustomization:
#     def __init__(self, color, name):
#         print(f"Zapuskaen klass{self.__class__}")
#         self.color = color
#         self.name = name
#
#     def __new__(cls, *args, **kwargs):
#         print(f"zapusk klass {cls}")
#         r = super().__new__(cls)
#         return r
#
#     def __del__(self):
#         pass
#
#     def __repr__(self):
#         pass
#
#     def __str__(self):
#         return f"{self.color} {self.name}"
#
#     def __bytes__(self):
#         pass
#
#     def __format__(self, format_spec):
#         pass
#
#     def __hash__(self):
#         pass
#
#     def __bool__(self):
#         pass
#
# a = BasicCustomization("red", "Anna")
# print(a)



# Sravnenie s __lt__
# class Dog:
#     def get_age(self):
#         return self.age
#
#     def __lt__(self, other):
#         if type(self) != type(other):
#             raise Exception('Incompatible argument to __lt__:' +str(other))
#
#         return self.get_age() < other.get_age()
#
# snoopy = Dog()
# snoopy.age = 4
# scooby = Dog()
# scooby.age = 6
# print(snoopy.__lt__(scooby))




# # __add__ __mul__ __sub__ __truediv__
# class BankAccount:
#     def __init__(self, name, balance):
#         print('new_obj init')
#         self.name = name
#         self.balance = balance
#
#     def __repr__(self):
#         return f'Klient {self.name} s balansom {self.balance}'
#
#     def __add__(self, other):
#         print('__add__ call')
#         if isinstance(other, BankAccount):
#             return self.balance + other.balance
#         if isinstance(other, (int, float)):
#             return BankAccount(self.name, self.balance+other)
#         if isinstance(other, (str)):
#             return self.name + other
#         raise NotImplemented
#
#     def __mul__(self, other):
#         print('__mul__ call')
#         if isinstance(other, BankAccount):
#             return self.balance * other.balance
#         if isinstance(other, (int, float)):
#             return self.balance * other
#         if isinstance(other, (str)):
#             return self.name + other
#         raise NotImplemented
#
#     def __radd__(self, other):
#         return self+other