#Напишите скрипт, который создаёт текстовый файл и записывает в него 10000 случайных действительных чисел.
# Создайте ещё один скрипт, который читает числа из файла и выводит на экран их сумму.

import random
with open("9.1.txt", 'w') as file:
    for i in range(10000):
        print(random.randint(1, 10001), file=file)

with open('9.1.txt', 'r', encoding='utf8') as file:
    numbers = [int(i) for i in file.readlines()]
    print(sum(numbers))
    print(*numbers)
