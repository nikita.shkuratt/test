# Создайте список товаров в интернет-магазине. Сериализуйте его при помощи pickle и сохраните в JSON.
import json
import pickle
'''Что означает сохраниите его в джонсон. Вывод с помощью джонсон?'''

# filename = '9.3.json'
# shop_list = ['phone', 'laptop', 'player', 'lighter', 'power-banks']
# with open(filename, 'wb') as file:
#     pickle.dump(shop_list, file)
#
# with open(filename, 'rb') as file:
#     print(pickle.load(file))



toys = {'doll': 'Barbie',
        'soft toy': 'bear',
        'ball': 'football',
        'creation': 'plasticine',
        'plastic toy': 'Lego'
        }

my_picked_object = pickle.dumps(toys)

with open('toys_list.json', 'w') as json_file:
    json.dump(str(my_picked_object), json_file)
