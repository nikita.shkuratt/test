g = [1, 2, 5, 8, 10, 55]


def generator(lst):
    for i in range(len(g)):
        if lst[i] ** 2 % 2 == 0:
            yield lst[i] ** 2


gen = generator(g)
print(list(gen))

print('-' * 90)
print([g[x] ** 2 for x in range(len(g))])

print('-' * 90)
print([g[x] ** 2 if g[x] ** 2 % 2 == 0 else None for x in range(len(g))])
