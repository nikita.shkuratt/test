###########################################################################################
# # Пример с вычислением количество гласных в тексте
# text = 'a b c d e fii g lmnioj'
# def get_count(sentence):
#     r1 = str(sentence).count('a')
#     r2 = str(sentence).count('e')
#     r3 = str(sentence).count('i')
#     r4 = str(sentence).count('o')
#     r5 = str(sentence).count('u')
#     #print(sum(c in 'aeiou' for c in sentence))
#     #return sum(1 for let in inputStr if let in "aeiouAEIOU")
# get_count(text)

# seven(times(five())) # must return 35
# four(plus(nine())) # must return 13
# eight(minus(three())) # must return 5
# six(divided_by(two())) # must return 3

###########################################################################################
# Пример с вычислением внутренности функций
# def identity(a): return a
#
# def zero(f=identity): return f(0)
# def one(f=identity): return f(1)
# def two(f=identity): return f(2)
# def three(f=identity): return f(3)
# def four(f=identity): return f(4)
# def five(f=identity): return f(5)
# def six(f=identity): return f(6)
# def seven(f=identity): return f(7)
# def eight(f=identity): return f(8)
# def nine(f=identity): return f(9)
#
# def plus(b): return lambda a: a + b
# def minus(b): return lambda a: a - b
# def times(b): return lambda a: a * b
# def divided_by(b): return lambda a: a // b
#
# print(nine(times(five())))

###########################################################################################
# Example
# If the input is summary=25, then the output must be n=17: The numbers 1 to 17 have 25 digits in total:
# 1234567891011121314151617.

# def amount_of_pages(summary):
#     chars = ''
#     for i in range(1, summary+1):
#         chars += str(i)
#         if len(chars) >= summary:
#             break
#     return i
#
#     #return (''.join(for chars in range(1, summary+1)) )
#
#
# print(amount_of_pages(25))

###########################################################################################
# Пример возведение в квадрат без *
# def square(n):
#     # res = n
#     # for i in range(1, n):
#     #     res += n
#     # return res
#
#     return sum(n for i in range(n))
#
# print(square(11))

###########################################################################################
# def gimme(input_array):
#     z, x, c = input_array[0], input_array[1], input_array[2]
#     if z < x < c or c < x < z:
#         return 1
#     elif x < z < c or c < z < x:
#         return 0
#     elif x < c < z or z < c < x:
#         return 2
#
#
# print(gimme(a))

###########################################################################################
# 15 3 6 9 12  5 10
# def solution(number=15):
#     b = 0
#     for i in range(number):
#         if i % 3 == 0 or i % 5==0:
#             b += i
#     return b
#     """Сокращение генератором
#     return sum(x for x in range(number) if x % 3 == 0 or x % 5 == 0)
#     """
# print(solution(15))

###########################################################################################
# Функция на переворот слов,которые длинее 5
# def spin_words(sentence='present will Kata more with words five Write returns word word'):
#     # com = ''
#     # for i in str(sentence).split():
#     #     if len(i) > 5:
#     #         i=i[::-1]
#     #     com += f'{i} '
#     return " ".join([x[::-1] if len(x) >= 5 else x for x in sentence.split(" ")])
#
# print(spin_words())

###########################################################################################
""""din"      =>  "((("
"recede"   =>  "()()()"
"Success"  =>  ")())())"
"(( @"     =>  "))(("
Testing for word "lfDEDZXNBDBVBN(HDbBh": '((((((((((' should equal '(()()(())))())()))))'"""
# def duplicate_encode(word="))(()())())"):
#     # wor = ''
#     # word = word.lower()
#     # for i in word:
#     #     if word.count(i) == 1:
#     #         wor += "("
#     #     else:
#     #         wor += ")"
#     # return wor
#     return ''.join(['(' if word.lower().count(x) == 1 else ")" for x in word.lower()])
#
# print(duplicate_encode())

###########################################################################################
# Функция на перемещение нулей в конец списка
# def move_zeros(array=[1,0,2,3,0,1,2,44,0,1,11]):
#     # for i in array:
#     #     if i == 0:
#     #         array.remove(i)
#     #         array.append(i)
#     # return array
#     return sorted(array, key=lambda x: x == 0, reverse=True)
# print(move_zeros())
# Testing random values: 337 ,-171 ,-219: '151-AB-DB' should equal 'FF0000'

###########################################################################################
# ИЗ инт в РГБ
# def rgb(r, g, b):
#     return ('{:X}{:X}{:X}').format(r, g, b)
#
####max(0, min(x, 255))#####
# print(rgb(337, -171, -219))

###########################################################################################
# 'Nibvq fhpprff ng nyy pbfgf!'
# def rot13(message='Avoid Success At All Costs!'):
#     alf = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
#     new_message = ""
#     for i in message:
#         if i == ' ':
#             new_message += ' '
#             continue
#         elif not i.isalpha():
#             new_message +=i
#             continue
#         elif i.isupper():
#             new_message += alf[alf.find(i.lower()) + 13].title()
#             continue
#         new_message += alf[alf.find(i)+13]
#     return new_message
#
# print(rot13())

###########################################################################################
# [[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [21, 22, 23, 24, 25]]
# should equal [1, 2, 3, 4, 5, 10, 15, 20, 25, 24, 23, 22, 21, 16, 11, 6, 7, 8, 9, 14, 19, 18, 17, 12, 13]

# arr1 = [[1,  2,  3],
#         [8, 9, 4],
#         [7, 6, 5]]
#
# arr2 = [[1,  2,  3,  4,  5],
#         [6,  7,  8,  9,  10],
#         [11, 12, 13, 14, 15],
#         [16, 17, 18, 19, 20],
#         [21, 22, 23, 24, 25]]
# def snail(array):
#     results = []
#     while len(array) > 0:
#         results.extend(array[0])
#         del array[0]
#         if len(array) > 0:
#             for i in array:
#                 results += [i[-1]]
#                 del i[-1]
#             if array[-1]:
#                 results.extend(reversed(array[-1]))
#                 del array[-1]
#                 for i in reversed(array):
#                     results+= [i[0]]
#                     del i[0]
#     return results
"""Очень интересный пример с переворотом списка,списков. В данной задаче можно сказать разворот на 90 градусов"""
# def snail(array):
#     out = []
#     while len(array):
#         out += array.pop(0)
#         array = list(zip(*array))[::-1] # Rotate
#     return out
# print(snail(arr2))

###########################################################################################
# Игра в кости Greed dice
"""hree 1's => 1000 points
 Three 6's =>  600 points
 Three 5's =>  500 points
 Three 4's =>  400 points
 Three 3's =>  300 points
 Three 2's =>  200 points
 One   1   =>  100 points
 One   5   =>   50 point"""
i = [1, 1, 1, 1, 1]
# def score(dice=[1, 1, 1, 1, 1]):
#     r = 0
#     if dice.count(1) >= 3:
#         r += 1000
#         dice.remove(1), dice.remove(1), dice.remove(1)
#     if dice.count(6) >= 3:
#         r += 600
#     if dice.count(5) >= 3:
#         r += 500
#     if dice.count(4) >= 3:
#         r += 400
#     if dice.count(3) >= 3:
#         r += 300
#     if dice.count(2) >= 3:
#         r += 200
#     if dice.count(1) == 1:
#         r += 100
#     if dice.count(1) == 2:
#         r += 200
#     if dice.count(5) == 1:
#         r += 50
#     if dice.count(5) == 2:
#         r += 100
#     return r

'''Интересная и простая версия'''
# def score(dice):
#     return dice.count(1)//3 * 1000 + dice.count(1)%3 * 100 \
#            + dice.count(2)//3 * 200 \
#            + dice.count(3)//3 * 300 \
#            + dice.count(4)//3 * 400 \
#            + dice.count(5)//3 * 500 + dice.count(5)%3 * 50 \
#            + dice.count(6)//3 * 600 \
#
# print(score(i))

###########################################################################################
"""Варианты добавления числа """


# class Chain:
#     def __init__(self, number):
#         self.number = number
#
#     def __call__(self, value=0):
#         return Chain(self.number + value)
#
#     def __str__(self):
#         return str(self.number)
#
#
# print(Chain(5)(2)(-10))
# f = lambda x: lambda y: x + y

# class Chain1(int): # если с инт,класс считается типом численным,за счет чего можем добавлять числа перед. Как ниже
#     def __call__(self, *args, additional=0):
#        return Chain1(self + additional)
#
# print(4+Chain1(1)(8))

###########################################################################################


'''exp_sum(1) # 1
exp_sum(2) # 2  -> 1+1 , 2
exp_sum(3) # 3 -> 1+1+1, 1+2, 3
exp_sum(4) # 5 -> 1+1+1+1, 1+1+2, 1+3, 2+2, 4
exp_sum(5) # 7 -> 1+1+1+1+1, 1+1+1+2, 1+1+3, 1+2+2, 1+4, 5, 2+3

exp_sum(10) # 42'''

#count = 0
# def exp_sum(n, p=[]):
#     global count
#     if n == 0:
#         count += 1
#         print(p)
#     else:
#         for i in range(1, n+1):
#             if not p:
#                 exp_sum(n-i, p+[i])
#             elif p[-1] >= i:
#                 exp_sum(n - i, p + [i])
#     return count
#
#
#
# print(exp_sum(10))

# def exp_sum(n):
#   if n < 0:
#     return 0
#   dp = [1]+[0]*n
#   for num in range(1,n+1):
#     for i in range(num,n+1):
#       dp[i] += dp[i-num]
#   return dp[-1]
#
#
# print(exp_sum(5))

###########################################################################################
#Basic: [1, 4, 8, 7, 3, 15] should return [1, 7] for sum = 8
# def sum_pairs(ints=[5, 9, 13, -3], s=10):
#     '''функция совзвращает сумму из списка 2-ух чисел которые равны s'''
#     for i in ints:
#         for j in ints[1:]:
#             if i+j==s:
#                 return [i], [j]
#
#
# def sum_pairs(lst=[10, 5, 2, 3, 7, 5], s=10):
#     cache = set()
#     for i in lst:
#         if s - i in cache:
#             return [s - i, i]
#         cache.add(i)

# def sum_pairs(ints=[10, 5, 2, 3, 7, 5], s=10):
#     if s%2:
#         ints = list(set(ints))
#     lst = []
#     for i in range(len(ints)):
#         for j in range(i+1, len(ints)):
#             if ints[i] + ints[j] == s:
#                 lst.append([ints[i], ints[j]])
#                 ints = ints[:j]
#                 break
#     return lst[-1] if lst else None
# print(sum_pairs())

###########################################################################################
"""Суть задачи вернуть индексной номер числа фибоначи: 0=[1],1=[2],1=[3],2=[4],3=[5],5=[6]..."""
# def fibbonaci(n):
#     lst = [0, 1, 1]
#     num1, num2 = 0, 1
#     for i in range(n):
#         num1, num2 = num2, num2 + num1
#         lst.append(num1+num2)
#     return lst[n-1]
# print(fibbonaci(5))
# def nth_fib(n):
#     if n <= 2:
#         return n-1
#     return nth_fib(n-1) + nth_fib(n-2)
#
# print(nth_fib(11))

###########################################################################################
