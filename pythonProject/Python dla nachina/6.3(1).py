def stupe(n):
    if n in (1, 2):
        return n
    else:
    #print(f"{n}-1 + {n}-2 =", n - 1 + n - 2,f'n={n}',f'n-1={n-1}',f'n-2={n-2}')
        return stupe(n - 1) + stupe(n - 2)  #4(3(2(2)+ 1(1)) 2) 3 3 1


print(stupe(8), "способами можно подняться на заданную ступеньку")