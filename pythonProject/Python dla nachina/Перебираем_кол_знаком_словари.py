#Пересчитываем одно значение
"""list = [1,1,2,2,3,3,3,4,4,4,4,4,5,5,5,5,5,5,5,6,7,8,8,]
value_to_count = 1
count_dict = {value_to_count: 0}
for element in list:
    if element == value_to_count:
        count_dict[value_to_count] += 1
print(count_dict)"""
#Пересчитываем все значения в словаре
list = [1,1,2,2,3,3,3,4,4,4,4,4,5,5,5,5,5,5,5,6,7,8,8,]
count_dict = {}
for element in list:
    if element in count_dict:
        count_dict[element] += 1
    else:
        count_dict[element] = 1
print(count_dict)

for key, value in count_dict.items():
    print("Key:", key, "count", value)

# Убираем со словаря слова которые начинаются на А
d = {"alica":35, "mark": 25, "april": 45, "john": 14}
new_d = {}
for name, age in d.items():
    if name[0] != "a":
        new_d[name] = age
print(new_d)