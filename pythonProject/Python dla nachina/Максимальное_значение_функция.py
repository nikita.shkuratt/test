def get_max_from_list(list_values):
    max_value = list_values[0]
    for element in list_values:
        if element > max_value:
            max_value = element
    return max_value


a = [-1,-2,-5,-22,-3,-1]
max_a = get_max_from_list(a)
print(max_a)