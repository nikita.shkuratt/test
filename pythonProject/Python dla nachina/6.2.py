#Создайте программу, которая проверяет, является ли палиндромом введённая фраза.
def is_palindrome(string):
    reversed_string = string[::-1]
    print(string == reversed_string)


is_palindrome('аргентинаманитнегра') # :D n
is_palindrome('аргентина')