a = 1
b = -8
c = 3
discr = b ** 2 - 4 * a * c

if discr <= 0:
    print("Значение дискриминанта отрицательное или равно нулю")
    x = -b / (2 * a)
    print("x =",x)
elif discr > 0:
    x1 = -b + (0.5 ** int(discr)) / (2 * a)
    x2 = -b - (0.5 ** int(discr)) / (2 * a)
    print("x1 =",x1)
    print("x2 =",x2)

x = -b / (2 * a)
result = (a * x ** 2) + (b * x) + c
print("Result =",float(result))
